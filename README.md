# hello-cdi

## Quick Start

If you haven't checked out this project onto your computer, you can do it now:

```sh
git clone https://gitlab.com/diskuv/samples/hello-cdi.git
cd hello-cdi
```

Then in either **PowerShell on Windows** or a **Unix shell** run:

```sh
./dk dksdk.coder.compile VERSION main EXPRESSION cde/Quickstart.ml POLL 0.5 OUTPUT cdi/Quickstart.cdi
```

The initial download and setup should proceed without prompting and take a few minutes.

After that it will automatically compile the file [cde/Quickstart.ml](cde/Quickstart.ml) whenever
that `.ml` file changes. The compilation is captured in [cdi/Quickstart.cdi](cdi/Quickstart.cdi) which is a
machine format not meant to be understood by us humans.

> Under construction! Yes ... there will be more documentation published.

## FAQs

### Q: Why does `.dk` pause for a couple seconds when it starts?

It is looking for updates. This doesn't need to be done every time `.dk` is launched but as a first pass it is ok.

### Q: Why is `.dk` using CMake instead of OCaml?

Eventually (read: years) `./dk` and `./dk.cmd` will use OCaml. In 2023 CMake was a much better scripting choice than OCaml. One command to download a file from the Internet, one option to do SHA-256 integrity checks, etc.
