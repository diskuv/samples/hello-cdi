(*************************************************************************
 * File: hello-cdi\cde\quickstart.ml                                     *
 *                                                                       *
 * Copyright 2024 Diskuv, Inc.                                           *
 *                                                                       *
 * Licensed under the Open Software License version 3.0                  *
 * (the "License"); you may not use this file except in compliance       *
 * with the License. You may obtain a copy of the License at             *
 *                                                                       *
 *     https://opensource.org/license/osl-3-0-php/                       *
 *                                                                       *
 *************************************************************************)

open DkSDKCoder_Std

let addv_staged =
  [%proc
    fun n vout v1 v2 ->
      for i = 0 to n - 1 do
        vout.(i) <- v1.(i) + v2.(i)
      done]

module E (I : Cdinst.Lang.SYM) = struct
  open I

  let blocks =
    [
      block
        (label ~project:"someproject" ~deployment:"somedeployment" "somename" [])
        [ declare (typeregister "Hi") unit; declare (typeregister "Bye") unit ]
        (transform ~name:"TestTransform"
           ~params:[ typeregister "Hi" ]
           ~transform:(Comp.compile addv_staged) ~langs:[]
           ~output:(typeregister "Bye") ());
    ]
end
